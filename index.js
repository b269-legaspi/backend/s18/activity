const addTwoNumbersFunction = function (num1, num2) {
  console.log(num1 + num2);
};
addTwoNumbersFunction(2, 5);

//

const subtractTwoNumbersFunction = function (num1, num2) {
  console.log(num1 - num2);
};
subtractTwoNumbersFunction(5, 2);

//

const multiplyTwoNumbersFunction = function (num1, num2) {
  return num1 * num2;
};

const product = multiplyTwoNumbersFunction(5, 2);
console.log(product);

//

const divideTwoNumbersFunction = function (num1, num2) {
  return num1 / num2;
};

const quotient = divideTwoNumbersFunction(5, 2);
console.log(quotient);

//

const areaOfACircleFunction = function (radius) {
  return Math.PI * radius ** 2;
};

const circleArea = areaOfACircleFunction(2);
console.log(circleArea);

//

const averageFunction = function (num1, num2, num3, num4) {
  return (num1 + num2 + num3 + num4) / 4;
};

const averageVar = averageFunction(88, 90, 92, 97);
console.log(averageVar);

const isPassed = function (score, total) {
  // console.log((score / total) * 100); // testing purposes
  return (score / total) * 100 > 75;
};

console.log(isPassed(19, 25));
console.log(isPassed(18, 25));

const isPassingScore = isPassed(19, 25);

console.log(isPassingScore);
